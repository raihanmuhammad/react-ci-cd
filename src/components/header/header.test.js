import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Header from "./header";

const CALL_API_MOCK = jest.fn();

describe("Kumpulan test contoh", () => {
  beforeEach(() => {
    render(<Header callAPI={CALL_API_MOCK} />);
  });
  it("Should be render text header", () => {
    const textBrand = screen.getByText(/nama brand/i);
    expect(textBrand).toBeInTheDocument();
  });

  test("btn apakah dia benar benar menjalankan function ketika di click", () => {
    const btnSubmit = screen.getByTestId("btn-submit");
    userEvent.click(btnSubmit);
    expect(CALL_API_MOCK).toHaveBeenCalled();
  });
});
