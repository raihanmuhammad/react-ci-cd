import { render, screen } from '@testing-library/react';
import App from './App';

// Search type
// ByText, ByRole

// Search variant
// GetBy, queryBy

test('renders learn react link', () => {
  render(<App />);
  const linkElement = screen.getByTestId("text-a");
  expect(linkElement).toBeInTheDocument();
});
